from dataclasses import dataclass


@dataclass
class Glossary:
    key: str
    short: str
    long: str
    expand: bool = True

    def export(self) -> str:
        line = rf"\newacronym{{{self.key}}}{{{self.short}}}{{{self.long}}}"
        if not self.expand:
            line = rf"{line}\glsunset{self.key}"
        return line


def gls(key: str, long: str, short: str | None = None) -> Glossary:
    if short is None:
        short = key.upper()
    return Glossary(key, short, long)


if __name__ == "__main__":
    from pathlib import Path

    glossaries: dict[str, Glossary] = {
        "cbb": gls("cbbc", "Calvin-Benson-Bassham cycle", "CBB cycle")
    }

    with open(Path("tex") / "glossaries.tex", "w+") as fp:
        fp.write(
            "\n".join(
                sorted(
                    rf"\newacronym{{{v.key}}}{{{v.short}}}{{{v.long}}}"
                    for v in glossaries.values()
                )
            )
        )
