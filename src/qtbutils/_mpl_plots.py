from __future__ import annotations

import itertools as it
import math
from typing import Any, cast

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import ticker
from matplotlib.axes import Axes as Axis
from matplotlib.figure import Figure
from mpl_toolkits.mplot3d.axes3d import Axes3D as Axis3D

from ._general import unwrap
from ._mpl_layouts import grid_layout
from ._type_vars import T
from ._types import PandasPlot


def _fig_ax_if_neccessary(
    ax: Axis | None,
    subplot_kw: dict[str, Any] | None = None,
) -> tuple[Figure, Axis]:
    if ax is None:
        return plt.subplots(constrained_layout=True, subplot_kw=subplot_kw)

    return unwrap(ax.get_figure()), ax


###############################################################################
# Groups
###############################################################################


def plot_groups(
    groups: list[pd.DataFrame] | list[pd.Series],
    *,
    plot_kind: PandasPlot = "line",
    n_cols: int = 2,
    col_width: float = 3,
    row_height: float = 4,
    sharex: bool = True,
    sharey: bool = False,
    grid: bool = True,
) -> tuple[Figure, list[Axis]]:
    fig, axs = grid_layout(
        len(groups),
        n_cols=n_cols,
        col_width=col_width,
        row_height=row_height,
        sharex=sharex,
        sharey=sharey,
    )

    for group, ax in zip(groups, axs):
        group.plot(ax=ax, kind=plot_kind, grid=grid)

    for i in range(len(groups), len(axs)):
        axs[i].set_visible(False)

    return fig, axs


###############################################################################
# Autogrid
###############################################################################


def _partition_by_order_of_magnitude(s: pd.Series) -> list[list[str]]:
    return [
        i.to_list()
        for i in np.floor(np.log10(s)).to_frame(name=0).groupby(0)[0].groups.values()  # type: ignore
    ]


def _split_large_groups(groups: list[list[T]], max_size: int) -> list[list[T]]:
    return list(
        it.chain(
            *(
                (
                    [group]
                    if len(group) < max_size
                    else [  # type: ignore
                        list(i)
                        for i in np.array_split(group, math.ceil(len(group) / max_size))  # type: ignore
                    ]
                )
                for group in groups
            )
        )
    )  # type: ignore


def plot_autogrid(
    s: pd.Series | pd.DataFrame,
    *,
    plot_kind: PandasPlot = "line",
    n_cols: int = 2,
    col_width: float = 4,
    row_height: float = 3,
    max_group_size: int = 6,
    grid: bool = True,
) -> tuple[Figure, list[Axis]]:
    if isinstance(s, pd.Series):
        group_names = _partition_by_order_of_magnitude(s)
    else:
        group_names = _partition_by_order_of_magnitude(s.max())

    group_names = _split_large_groups(group_names, max_size=max_group_size)

    groups: list[pd.Series] | list[pd.DataFrame]

    if isinstance(s, pd.Series):
        groups = [s.loc[group] for group in group_names]
    else:
        groups = [s.loc[:, group] for group in group_names]

    return plot_groups(
        groups,
        plot_kind=plot_kind,
        n_cols=n_cols,
        col_width=col_width,
        row_height=row_height,
        grid=grid,
    )


###############################################################################
# 3D plots
###############################################################################


def _index2d_to_meshgrid(
    df: pd.DataFrame,
    nx: int | None = None,
    ny: int | None = None,
) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    l1 = df.index.get_level_values(1)
    l0 = df.index.get_level_values(0)

    if nx is None:
        nx = len(l1.unique())
    if ny is None:
        ny = len(l0.unique())

    x = l1.to_numpy().reshape(-1, nx)
    y = l0.to_numpy().reshape(ny, -1)
    z = df.to_numpy().reshape(ny, nx)
    return x, y, z


def _df_to_meshgrid(
    df: pd.DataFrame,
) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    xs = df.index
    ys = df.columns

    nx = len(xs)
    ny = len(ys)

    x = xs.to_numpy().reshape(-1, nx)
    y = ys.to_numpy().reshape(ny, -1)
    z = df.to_numpy().reshape(ny, nx)
    return x, y, z


def plot_surface(
    df: pd.DataFrame, key: str, ax: Axis | Axis3D | None = None
) -> tuple[Figure, Axis3D]:
    df = df[key].unstack()
    fig, ax = _fig_ax_if_neccessary(ax, subplot_kw={"projection": "3d"})
    ax = cast(Axis3D, ax)

    ax.plot_surface(*_df_to_meshgrid(df), cmap="viridis")
    ax.set_xlabel(df.index.name)
    ax.set_ylabel(df.columns.name)
    ax.set_zlabel(key, labelpad=-0.2)
    ax.set_box_aspect(
        aspect=None,
        zoom=cast(int, 0.9),
    )

    ax.xaxis.set_major_locator(ticker.MaxNLocator(4))
    ax.yaxis.set_major_locator(ticker.MaxNLocator(4))
    ax.zaxis.set_major_locator(ticker.MaxNLocator(4))
    return fig, ax
