# QTB Utils

[![pipeline status](https://gitlab.com/marvin.vanaalst/qtb-plot/badges/main/pipeline.svg)](https://gitlab.com/marvin.vanaalst/qtb-plot/-/commits/main)
[![coverage report](https://gitlab.com/marvin.vanaalst/qtb-plot/badges/main/coverage.svg)](https://gitlab.com/marvin.vanaalst/qtb-plot/-/commits/main)
[![PyPi](https://img.shields.io/pypi/v/qtb-plot)](https://pypi.org/project/qtb-plot/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Downloads](https://pepy.tech/badge/qtb-plot)](https://pepy.tech/project/qtb-plot)

