from __future__ import annotations

import itertools as it
from dataclasses import dataclass
from functools import partial
from typing import Any, Callable, Collection, Generator, Iterable, Type, cast

import numpy as np
import pandas as pd
from modelbase.ode import Model, Simulator

from ._parallel import parallelise
from ._type_vars import Tin, Tout

InitialValues = dict[str, float]


@dataclass
class TimePoint:
    concs: pd.Series
    fluxes: pd.Series

    def __iter__(self) -> Generator[pd.Series, None, None]:
        yield self.concs
        yield self.fluxes

    @staticmethod
    def _default_concs(model: Model, concs: pd.DataFrame | None) -> pd.Series:
        if concs is None:
            return pd.Series(
                data=np.zeros(len(model.compounds)),
                index=model.compounds,
            )
        return concs.iloc[-1]

    @staticmethod
    def _default_fluxes(model: Model, fluxes: pd.DataFrame | None) -> pd.Series:
        if fluxes is None:
            return pd.Series(
                data=np.zeros(len(model.rates)),
                index=list(model.rates.keys()),
            )  # type: ignore
        return fluxes.iloc[-1]

    @classmethod
    def default(
        cls: Type[TimePoint],
        model: Model,
        *,
        concs: pd.DataFrame | None = None,
        fluxes: pd.DataFrame | None = None,
    ) -> TimePoint:
        return cls(
            concs=cls._default_concs(model, concs),
            fluxes=cls._default_fluxes(model, fluxes),
        )


@dataclass
class TimeCourse:
    concs: pd.DataFrame
    fluxes: pd.DataFrame

    def __iter__(self) -> Generator[pd.DataFrame, None, None]:
        yield self.concs
        yield self.fluxes

    @staticmethod
    def _default_concs(
        model: Model,
        concs: pd.DataFrame | None,
        time_points: Collection[float],
    ) -> pd.DataFrame:
        if concs is None:
            return pd.DataFrame(
                data=np.zeros((len(time_points), len(model.compounds))),
                columns=model.compounds,
            )
        return concs

    @staticmethod
    def _default_fluxes(
        model: Model,
        fluxes: pd.DataFrame | None,
        time_points: Collection[float],
    ) -> pd.DataFrame:
        if fluxes is None:
            return pd.DataFrame(
                data=np.zeros((len(time_points), len(model.rates))),
                columns=list(model.rates.keys()),
            )
        return fluxes

    @classmethod
    def default(
        cls: Type[TimeCourse],
        model: Model,
        *,
        time_points: Collection[float],
        concs: pd.DataFrame | None = None,
        fluxes: pd.DataFrame | None = None,
    ) -> TimeCourse:
        return cls(
            concs=cls._default_concs(model, concs, time_points),
            fluxes=cls._default_fluxes(model, fluxes, time_points),
        )


@dataclass
class SteadyStateScan:
    concs: pd.DataFrame
    fluxes: pd.DataFrame

    def __iter__(self) -> Generator[pd.DataFrame, None, None]:
        yield self.concs
        yield self.fluxes


@dataclass
class TimeCourseScan:
    concs: pd.DataFrame
    fluxes: pd.DataFrame

    def __iter__(self) -> Generator[pd.DataFrame, None, None]:
        yield self.concs
        yield self.fluxes


def _simulate_time_course(
    model: Model, y0: dict[str, float], time_points: Collection[float]
) -> TimeCourse:
    concs, fluxes = (
        Simulator(model)
        .initialise(y0)
        .simulate_and(time_points=cast(list, time_points))
        .get_full_results_and_fluxes_df()
    )
    return TimeCourse.default(model, concs=concs, fluxes=fluxes, time_points=time_points)


def _simulate_to_steady_state(model: Model, y0: dict[str, float]) -> TimePoint:
    concs, fluxes = (
        Simulator(model)
        .initialise(y0)
        .simulate_to_steady_state_and()
        .get_full_results_and_fluxes_df()
    )
    return TimePoint.default(model, concs=concs, fluxes=fluxes)


def _wrap_simulation_with_input(
    inp: Tin,
    *,
    model: Model,
    perturb_fn: Callable[[Model, Tin], Model],
    simulation_fn: Callable[[Model], Tout],
) -> tuple[Tin, Tout]:
    return inp, simulation_fn(perturb_fn(model, inp))


def _scan_over_iterable(
    model: Model,
    *,
    perturb_fn: Callable[[Model, Tin], Model],
    simulation_fn: Callable[[Model], Tout],
    inputs: Collection[Tin],
) -> dict[Tin, Tout]:
    return parallelise(
        partial(
            _wrap_simulation_with_input,
            model=model,
            perturb_fn=perturb_fn,
            simulation_fn=simulation_fn,
        ),
        inputs,
    )


def scan_ss_over_iterable(
    model: Model,
    y0: InitialValues,
    perturb_fn: Callable[[Model, Any], Model],
    inputs: dict[str, Iterable[Any]],
) -> SteadyStateScan:
    if len(inputs) == 1:
        values = list(inputs.values())[0]
    else:
        values = list(it.product(*inputs.values()))

    res = _scan_over_iterable(
        model,
        perturb_fn=perturb_fn,
        simulation_fn=partial(_simulate_to_steady_state, y0=y0),
        inputs=values,  # type: ignore
    )

    concs = pd.DataFrame({k: v.concs for k, v in res.items()}).T
    fluxes = pd.DataFrame({k: v.fluxes for k, v in res.items()}).T
    concs.index.names = list(inputs.keys())
    fluxes.index.names = list(inputs.keys())
    return SteadyStateScan(
        concs=concs,
        fluxes=fluxes,
    )


def scan_time_course_over_iterable(
    model: Model,
    y0: InitialValues,
    time_points: Collection[float],
    perturb_fn: Callable[[Model, Any], Model],
    inputs: dict[str, Iterable[Any]],
) -> TimeCourseScan:
    if len(inputs) == 1:
        values = list(inputs.values())[0]
    else:
        values = list(it.product(*inputs.values()))

    res = _scan_over_iterable(
        model,
        perturb_fn=perturb_fn,
        simulation_fn=partial(_simulate_time_course, y0=y0, time_points=time_points),
        inputs=values,  # type: ignore
    )

    concs = pd.DataFrame({k: v.concs for k, v in res.items()}).T
    fluxes = pd.DataFrame({k: v.fluxes for k, v in res.items()}).T
    concs.index.names = list(inputs.keys())
    fluxes.index.names = list(inputs.keys())

    return TimeCourseScan(
        concs=concs,
        fluxes=fluxes,
    )
