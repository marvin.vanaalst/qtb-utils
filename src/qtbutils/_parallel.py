from __future__ import annotations

import multiprocessing
import pickle  # nosec
import sys
from concurrent.futures import TimeoutError
from pathlib import Path
from typing import Callable, Collection, cast

import pebble
from tqdm import tqdm

from ._general import default_if_none
from ._type_vars import Ti, Tin, Tout


def load_or_run(inp: Ti, fn: Callable[[Ti], Tout], tmp_dir: Path = Path("tmp")) -> Tout:
    tmp_dir.mkdir(parents=True, exist_ok=True)

    file = tmp_dir / "{}.p".format(", ".join(f"{i:.2e}" for i in inp))

    if file.exists():
        with file.open("rb") as fp:
            return cast(Tout, pickle.load(fp))  # nosec

    res = fn(inp)
    with file.open("wb") as fp:
        pickle.dump(res, fp)

    return res


def parallelise(
    fn: Callable[[Tin], tuple[Tin, Tout]],
    inputs: Collection[Tin],
    *,
    max_workers: int | None = None,
    timeout: float | None = None,
    disable_tqdm: bool = False,
    tqdm_desc: str | None = None,
) -> dict[Tin, Tout]:
    results: dict[Tin, Tout]
    if sys.platform in ["win32", "cygwin"]:
        results = dict(
            tqdm(
                map(fn, inputs),
                total=len(inputs),
                disable=disable_tqdm,
                desc=tqdm_desc,
            )
        )
    else:
        results = {}
        max_workers = default_if_none(max_workers, multiprocessing.cpu_count() - 1)

        with tqdm(
            total=len(inputs),
            disable=disable_tqdm,
            desc=tqdm_desc,
        ) as pbar, pebble.ProcessPool(max_workers=max_workers) as pool:
            future = pool.map(fn, inputs, timeout=timeout)
            it = future.result()
            while True:
                try:
                    key, value = next(it)
                    pbar.update(1)
                    results[key] = value
                except StopIteration:  # noqa: PERF203
                    break
                except TimeoutError:
                    pbar.update(1)
    return results
