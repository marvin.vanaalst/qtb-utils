from typing import cast

import numpy as np

from ._types import Array


def find_nearest(a: Array, val: float) -> float:
    return cast(float, a[np.abs(a - val).argmin()])  # type: ignore
