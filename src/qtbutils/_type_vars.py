from typing import Hashable, Iterable, TypeVar

T = TypeVar("T")
Tin = TypeVar("Tin")
Tout = TypeVar("Tout")
T1 = TypeVar("T1")
T2 = TypeVar("T2")
Ti = TypeVar("Ti", bound=Iterable)
Thash = TypeVar("Thash", bound=Hashable)
