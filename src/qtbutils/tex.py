from __future__ import annotations

from pathlib import Path
from typing import Callable

right_arrow = r"\xrightarrow{}"
newline = r"\\" + "\n"
floatbarrier = r"\FloatBarrier"


def part(s: str) -> str:
    # depth = -1
    return floatbarrier + rf"\part{{{s}}}"


def chapter(s: str) -> str:
    # depth = 0
    return floatbarrier + rf"\part{{{s}}}"


def section(s: str) -> str:
    # depth = 1
    return floatbarrier + rf"\section{{{s}}}"


def section_(s: str) -> str:
    # depth = 1
    return floatbarrier + rf"\section*{{{s}}}"


def subsection(s: str) -> str:
    # depth = 2
    return floatbarrier + rf"\subsection{{{s}}}"


def subsection_(s: str) -> str:
    # depth = 2
    return floatbarrier + rf"\subsection*{{{s}}}"


def subsubsection(s: str) -> str:
    # depth = 3
    return floatbarrier + rf"\subsubsection{{{s}}}"


def subsubsection_(s: str) -> str:
    # depth = 3
    return floatbarrier + rf"\subsubsection*{{{s}}}"


def paragraph(s: str) -> str:
    # depth = 4
    return rf"\paragraph{{{s}}}"


def subparagraph(s: str) -> str:
    # depth = 5
    return rf"\subparagraph{{{s}}}"


def math_il(s: str) -> str:
    return f"${s}$"


def math(s: str) -> str:
    return f"$${s}$$"


def mathrm(s: str) -> str:
    return rf"\mathrm{{{s}}}"


def bold(s: str) -> str:
    return rf"\textbf{{{s}}}"


def clearpage() -> str:
    return r"\clearpage"


def list_with_headers(
    rows: list[tuple[str, str]],
    sec_fn: Callable[[str], str],
) -> str:
    return "\n\n".join(
        [
            "\n".join(
                (
                    sec_fn(name),
                    content,
                )
            )
            for name, content in rows
        ]
    )


def list_as_bold(rows: list[tuple[str, str]]) -> str:
    return "\n\n".join(
        [
            "\n".join(
                (
                    bold(name) + r"\\",
                    content,
                    r"\vspace{20pt}",
                )
            )
            for name, content in rows
        ]
    )


def figure(
    path: Path,
    caption: str,
    label: str,
    width: str = r"\linewidth",
) -> str:
    return rf"""\begin{{figure}}
  \centering
  \includegraphics[width={width}]{{{path.as_posix()}}}
  \caption{{{caption}}}
  \label{{{label}}}
\end{{figure}}
"""
    # return (
    #     r"\begin{figure}\n"
    #     r"\centering\n"
    #     rf"\includegraphics[width=\linewidth]{{{path}}}\n"
    #     rf"\caption{{{caption}}}\n"
    #     rf"\label{{{caption}}}\n"
    #     r"\end{figure}\n"
    # )
    # return textwrap.dedent(
    #     rf"""
    #   \begin{{figure}}
    #     \centering
    #     \includegraphics[width=\linewidth]{{{path.as_posix()}}}
    #     \caption{{{caption}}}
    #     \label{{{label}}}
    #   \end{{figure}}
    #   """
    # )[1:]


# def dataframe(df: DataFrame) -> str: ...
# def series(df: Series) -> str: ...
