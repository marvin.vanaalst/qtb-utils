from __future__ import annotations

import math
from typing import Any

import matplotlib.pyplot as plt
from matplotlib.axes import Axes as Axis
from matplotlib.figure import Figure

from ._general import unwrap


def _fig_ax_if_neccessary(
    ax: Axis | None,
    subplot_kw: dict[str, Any] | None = None,
) -> tuple[Figure, Axis]:
    if ax is None:
        return plt.subplots(constrained_layout=True, subplot_kw=subplot_kw)

    return unwrap(ax.get_figure()), ax


def grid_layout(
    n_groups: int,
    *,
    n_cols: int = 2,
    col_width: float = 3,
    row_height: float = 4,
    sharex: bool = True,
    sharey: bool = False,
) -> tuple[Figure, list[Axis]]:
    n_cols = min(n_groups, n_cols)
    n_rows = math.ceil(n_groups / n_cols)
    fig, axs = plt.subplots(
        n_rows,
        n_cols,
        figsize=(n_cols * col_width, n_rows * row_height),
        sharex=sharex,
        sharey=sharey,
        layout="constrained",
        squeeze=False,
    )
    return fig, list(axs.flatten())
