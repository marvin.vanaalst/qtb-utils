from dataclasses import dataclass
from typing import Generic, cast

from ._type_vars import T


@dataclass
class Option(Generic[T]):
    value: T | None

    def unwrap(self) -> T:
        assert (val := self.value) is not None
        return val


@dataclass
class Result(Generic[T]):
    value: T | Exception

    def is_ok(self) -> bool:
        return not isinstance(self.value, Exception)

    def err(self) -> Exception:
        return cast(Exception, self.value)

    def unwrap(self) -> T:
        val = self.value
        assert not isinstance(val, Exception)
        return val


if __name__ == "__main__":
    match Option(True).value:
        case True:
            print("True")
        case False:
            print("False")
        case None:
            print("None")

    r1 = Result(1.0)
    _ = r1.unwrap()
