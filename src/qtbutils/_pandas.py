from typing import overload

import numpy as np
import pandas as pd


def zero_to_nan(df: pd.DataFrame) -> pd.DataFrame:
    df = df.copy()
    df[df == 0.0] = np.nan
    return df


@overload
def rel_diff(val: float, ref: float) -> float: ...


@overload
def rel_diff(val: pd.Series, ref: pd.Series) -> pd.Series: ...


@overload
def rel_diff(val: pd.DataFrame, ref: pd.DataFrame) -> pd.DataFrame:  # type: ignore[misc]
    ...


def rel_diff(
    val: float | pd.Series | pd.DataFrame, ref: float | pd.Series | pd.DataFrame
) -> float | pd.Series | pd.DataFrame:
    return (val - ref) * 100 / ref


@overload
def rel_amount(part: float, total: float) -> float: ...


@overload
def rel_amount(part: pd.Series, total: pd.Series) -> pd.Series: ...


@overload
def rel_amount(part: pd.DataFrame, total: pd.Series) -> pd.DataFrame:  # type: ignore[misc]
    ...


@overload
def rel_amount(part: pd.Series, total: pd.DataFrame) -> pd.DataFrame:  # type: ignore[misc]
    ...


@overload
def rel_amount(part: pd.DataFrame, total: pd.DataFrame) -> pd.DataFrame:  # type: ignore[misc]
    ...


def rel_amount(
    part: float | pd.Series | pd.DataFrame, total: float | pd.Series | pd.DataFrame
) -> float | pd.Series | pd.DataFrame:
    return part * 100 / total
