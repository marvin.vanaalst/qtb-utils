from typing import Literal, TypeAlias

import numpy as np
from numpy.typing import NDArray

__all__ = [
    "Array",
    "Axis",
    "Axes",
    "PandasPlot",
    "PlotFormat",
]

from matplotlib.axes import Axes as Axis

Axes: TypeAlias = list[list[Axis]]

PandasPlot: TypeAlias = Literal[
    "line",
    "bar",
    "barh",
    "hist",
    "box",
    "kde",
    "density",
    "area",
    "pie",
    "scatter",
    "hexbin",
]
PlotFormat: TypeAlias = Literal["png", "pdf", "svg", "eps"]

Array = NDArray[np.float64]
