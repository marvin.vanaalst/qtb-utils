from __future__ import annotations

from pathlib import Path
from typing import Literal, cast

from matplotlib.axes import Axes as Axis
from matplotlib.figure import Figure
from matplotlib.ticker import PercentFormatter

from ._general import unwrap
from ._types import PlotFormat


def format_xticklabels(
    ax: Axis,
    rotation: float | None = None,
    align: Literal["center", "right", "left"] = "center",
) -> None:
    ax.set_xticks(
        ax.get_xticks(),
        cast(list[str], ax.get_xticklabels()),
        rotation=rotation,
        ha=align,
    )


def format_yticklabels(
    ax: Axis,
    rotation: float | None = None,
    align: Literal["center", "top", "bottom"] = "center",
) -> None:
    ax.set_yticks(
        ax.get_yticks(),
        cast(list[str], ax.get_yticklabels()),
        rotation=rotation,
        va=align,
    )


def legend_outside(ax: Axis, title: str | None = None) -> Axis:
    ax.legend(loc="upper left", bbox_to_anchor=(1.01, 1), borderaxespad=0, title=title)
    return ax


def percentage_xticks(
    ax: Axis,
    *,
    xmax: float = 1.0,
    decimals: int = 0,
) -> Axis:
    ax.xaxis.set_major_formatter(PercentFormatter(xmax=xmax, decimals=decimals))
    return ax


def percentage_yticks(
    ax: Axis,
    *,
    xmax: float = 1.0,
    decimals: int = 0,
) -> Axis:
    ax.yaxis.set_major_formatter(PercentFormatter(xmax=xmax, decimals=decimals))
    return ax


def percentage_ticks(
    ax: Axis,
    *,
    xaxis: bool = False,
    yaxis: bool = True,
    xmax: float = 1.0,
    decimals: int = 0,
) -> Axis:
    if xaxis:
        percentage_xticks(ax, xmax=xmax, decimals=decimals)
    if yaxis:
        percentage_yticks(ax, xmax=xmax, decimals=decimals)
    return ax


def savefig(
    plot: Figure | Axis,
    filename: str,
    path: Path = Path("img"),
    file_format: PlotFormat = "png",
    transparent: bool = False,
    dpi: float = 200,
) -> Path:
    path.mkdir(exist_ok=True, parents=True)

    if isinstance(plot, Figure):
        fig = plot
    else:
        fig = unwrap(plot.get_figure())

    filepath = path / f"{filename}.{file_format}"

    fig.savefig(
        filepath,
        bbox_inches="tight",
        transparent=transparent,
        dpi=dpi,
    )
    return filepath
